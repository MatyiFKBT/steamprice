const market = require("steam-market-pricing");

const names = ["Killstreak Tide Turner Kit", "Killstreak Tide Turner"];

market
  .getItemsPrices(440, names, 3)
  .then(items =>
    names.forEach(
        name => {
            if(!name.includes("Kit")){
                let price = parseFloat(items[name].lowest_price.slice(0,-1).replace(",","."))*0.85;
                console.log(`${name}: ${price}`)
            } else {
                console.log(`${name}: ${items[name].lowest_price}`)
            }
        }
    )
  );
